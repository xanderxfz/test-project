package ru.ekb.app;

/**
 * Класс для сортировки слов по номеру в конце строки
 * @author xanderxfz
 *
 */
public class OwnComparator implements Comparable<Integer>
{
    public static int compare(int x, int y)
    {
        return (x > y) ? -1 : ((x == y) ? 0 : 1);
    }

    public static int compare(String f, String s)
    {
        Integer x = Integer.valueOf(f.split(" ")[1]);
        Integer y = Integer.valueOf(s.split(" ")[1]);
        return compare(x, y);
    }

    @Override
    public int compareTo(Integer o)
    {
        return 0;
    }
}
