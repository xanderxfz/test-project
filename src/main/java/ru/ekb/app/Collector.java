package ru.ekb.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Класс для формирования карты <исходное слово, набор подходящих слов>
 * @author xanderxfz
 *
 */
public class Collector implements Runnable
{
    private final String sourceString;
    private Map<String, ArrayList<String>> dictionary;
    private Collection<String> wordsCollection;

    Collector(String sourceString, Map<String, ArrayList<String>> dictionary, Collection<String> wordsCollection)
    {
        this.sourceString = sourceString;
        this.dictionary = dictionary;
        this.wordsCollection = wordsCollection;
    }

    @Override
    public void run()
    {
        if (!dictionary.containsKey(sourceString))
        {
            try
            {
                ArrayList<String> words = new ArrayList<String>();

                wordsCollection.stream().filter(ln -> ln.startsWith(sourceString))
                        .sorted((s1, s2) -> OwnComparator.compare(s1, s2)).limit(10)
                        .forEach(l -> words.add(l.split(" ")[0]));
                dictionary.put(sourceString, words);
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
}
