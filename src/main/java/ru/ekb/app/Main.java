package ru.ekb.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Основной класс поекта
 * @author xanderxfz
 *
 */
public class Main
{
    private static final int THREADS = 8;
    private static final String filePatch = "test.in";

    private static Map<String, ArrayList<String>> dictionary = new ConcurrentHashMap<String, ArrayList<String>>();

    private static ArrayList<String> fileWords = new ArrayList<String>();
    private static Collection<String> wordsCollection;
    private static Collection<String> fileContent;

    /**
     * Стартовый метод
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException
    {
        /* Создание коллекции строка файла */
        try (Stream<String> file = Files.lines(Paths.get(filePatch)))
        {
            fileContent = file.collect(Collectors.toCollection(LinkedList::new));
        }

        createDictionary();
        printWords();
    }

    /**
     * Метод создания словаря
     */
    private static void createDictionary()
    {
        /* Получение количества слов */
        Long totalWords = Long.valueOf(fileContent.stream().findFirst().get());

        InitWordsCollection(totalWords);

        /* В многопоточном режиме проходим по всем строкам ниже N + 2 и фогмируем для каждой из
         *  них наборы из десяти возможных вариантов*/
        ExecutorService executor = Executors.newFixedThreadPool(THREADS);
        fileContent.stream().skip(totalWords + 2).forEach(word -> dictionaryFilling(word, executor));

        /* Ожидаем окончания работы всех потоков */
        executor.shutdown();
        while (!executor.isTerminated())
        {
        }
    }

    /**
     * Запуск потока наполнение словаря
     * @param word исходное слово
     * @param executor 
     */
    private static void dictionaryFilling(String word, ExecutorService executor)
    {
        /* Добавляем слово в список пройденных слов и запускаем поток подбора слов */
        fileWords.add(word);
        executor.execute(new Collector(word, dictionary, wordsCollection));
    }

    /**
     * Создание коллекции из строк с 1й строки по N + 2
     * @param lastDictWord количество уникальных слов
     */
    private static void InitWordsCollection(long lastDictWord)
    {
        wordsCollection = fileContent.stream().skip(1).limit(lastDictWord)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Выводим в консоль подобранные слова
     */
    private static void printWords()
    {
        for (String wo : fileWords)
        {
            System.out.println("\n");
            for (String el : dictionary.get(wo))
            {
                System.out.println(el);
            }
        }
    }
}
